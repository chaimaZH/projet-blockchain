// Load env variables
if (process.env.NODE_ENV != "production") {
  require("dotenv").config();
}

// Import dependencies
const express = require("express");
const cors = require("cors");
const connectToDb = require("./config/connectToDb");

const MarinsController = require("./Controller/marinsController");
const app = express();
const port = 3001;
// configuire express app
app.use(express.json());

app.use(cors());

// Connect to database
connectToDb();
// Routing
app.post("/Marin/ajouter", MarinsController.createmarin);

app.get("/Marin/lister", MarinsController.listermarins);
app.get("/Marin/:id", MarinsController.listermarin);
app.put("/Marin/:id", MarinsController.updatemarin);
app.delete("/Marin/:id", MarinsController.deleatemarin);
// Start our server
app.listen(process.env.PORT);
