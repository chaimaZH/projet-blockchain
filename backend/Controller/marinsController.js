const MarinModel = require("../Models/Marin");
const createmarin = async (req, res) => {
  //les traitment pour ajouter un noveau contact
  //he4a objet

  const lieu = req.body.lieu;
  const chlorophylle = req.body.chlorophylle;
  const oxygene = req.body.oxygene;
  const azote = req.body.azote;
  const phosphor = req.body.phosphor;

  //he4a model
  const Marin = await MarinModel.create({
    lieu: lieu,
    chlorophylle: chlorophylle,
    oxygene: oxygene,
    azote: azote,
    phosphor: phosphor,
  });
  res.json({ Marin: Marin });
};
const listermarins = async (req, res) => {
  const marins = await MarinModel.find().sort({ temps: 1 });
  res.json({ marins: marins });
};

const listermarin = async (req, res) => {
  // get id off the url
  const marinId = req.params.id;
  //find the marin using that id
  const marin = await MarinModel.findById(marinId);
  //Respond the whith the marin
  res.json({ marin: marin });
};
const updatemarin = async (req, res) => {
  // get id off the url
  const marinId = req.params.id;
  // get the data of the req body
  const lieu = req.body.lieu;
  const chlorophylle = req.body.chlorophylle;
  const oxygene = req.body.oxygene;
  const azote = req.body.azote;
  const phosphor = req.body.phosphor;
  //find the marin and update using that id
  await MarinModel.findByIdAndUpdate(marinId, {
    lieu: lieu,
    chlorophylle: chlorophylle,
    oxygene: oxygene,
    azote: azote,
    phosphor: phosphor,
  });
  //find updated marin
  const marin = await MarinModel.findById(marinId);
  //Respond the whith the marin
  res.json({ marin: marin });
};
const deleatemarin = async (req, res) => {
  // get id off the url
  const marinId = req.params.id;
  // delete the record
  await MarinModel.deleteOne({ id: marinId });
  // respond
  res.json({ success: "Marin deleated" });
};
module.exports = {
  createmarin: createmarin,
  listermarins: listermarins,
  listermarin: listermarin,
  updatemarin: updatemarin,
  deleatemarin: deleatemarin,
};
