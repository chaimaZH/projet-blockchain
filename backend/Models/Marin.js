const mongoose=require('mongoose')
// on définie schéma du collection 
// id yetesna3 par défaut 
const MarinSchema = new mongoose.Schema({
    lieu: {
            type : String,
            required : true
        },
    chlorophylle: {
            type : String,
            required : true
        },
    oxygene : {
            type : String,
            required : true
        },
    azote : {
            type : String,
            required : true
        },  
    phosphor : {
            type : String,
            required : true
        }, 
    

},{timestamps : true})

// exporter le shéma 
//collection contact te5o shéma mte3ha min contactSchema
module.exports = mongoose.model('Marins',MarinSchema)