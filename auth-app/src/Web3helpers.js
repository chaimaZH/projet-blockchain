import Web3 from "web3/dist/web3.min.js";
import Auth from "./build/contracts/Auth.json";

export const loadWeb3 = async () => {
  try {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum);
      await window.ethereum.request({ method: "eth_requestAccounts" });
    } else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider);
    } else {
      window.alert(
        "Non-Ethereum browser detected. You should consider trying MetaMask!"
      );
    }
  } catch (error) {
    console.error("Error loading Web3:", error);
  }
};

export const loadBlockchainData = async () => {
  const web3 = window.web3;
  // Load account
  const accounts = await web3.eth.getAccounts();
  // Network ID

  const networkId = await web3.eth.net.getId();
  console.log(networkId);

  // Network data
  console.log("auth adress:", Auth.networks[networkId].address);
  if (networkId) {
    const auth = new web3.eth.Contract(
      Auth.abi,
      Auth.networks[networkId].address
    );

    return { auth, accounts: accounts[0] };
  }
};
