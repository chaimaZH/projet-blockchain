import React from "react";
import { useNavigate } from "react-router-dom";
import { ArrowPathIcon, CloudArrowUpIcon, FingerPrintIcon, LockClosedIcon } from '@heroicons/react/24/outline'

const features = [
  {
    name: "Reducing Plastic Consumption",
    description:
      " Plastic has become ubiquitous in our lives, but there are various solutions to minimize its impact. Another step to consider is identifying unnecessary packaging and opting for reusable items like water bottles, bags, and containers.",
    icon: CloudArrowUpIcon,
  },
  {
    name: "Avoiding Plastic Trays Suitable for Microwaving",
    description:
      "The suggestion is to urge industries to find alternative solutions, similar to the approach taken with plastic bags in 2016. Pascal Canfin proposes prohibiting companies from introducing new products to the market that are not recyclable.",
    icon: LockClosedIcon,
  },
  {
    name: "In the sea, opt for low-impact activities",
    description:
      "Motorized water activities (jet skiing, towed sports, etc.) can significantly disturb marine biodiversity. Choose low-impact activities such as diving, canoeing, or surfing. Similarly, observation activities should be conducted under specific conditions, allowing you to explore the richness of marine biodiversity without posing a risk to it.",
    icon: ArrowPathIcon,
  },
  {
    name: "Do not throw anything into the sea",
    description:
      "Not throwing anything into the sea is a gesture that seems to be common sense, but given the numerous waste left on the beaches, particularly by summer tourists, sea history specialist Christian Buchet would like to point out. Because it is at the source, at the start of the journey, that we must intervene and not put anything into the sea. We must act immediately and there is no pointless action, encouraging citizens not to lower hands in the fight against plastic pollution.",
    icon: FingerPrintIcon,
  },
];
export default function Home({ updateEmail })  {
  const navigate = useNavigate();
  return (
    <div className="bg-white py-24 sm:py-32">
      <div className="mx-auto max-w-7xl px-6 lg:px-8">
        <div className="mx-auto max-w-2xl lg:text-center">
          <p className="mt-2 text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
            Practical Tips for Pollution Protection
          </p>
          <p className="mt-6 text-lg leading-8 text-gray-600">
            Safeguarding Our Environment
          </p>
        </div>
        <div className="mx-auto mt-16 max-w-2xl sm:mt-20 lg:mt-24 lg:max-w-4xl">
          <dl className="grid max-w-xl grid-cols-1 gap-x-8 gap-y-10 lg:max-w-none lg:grid-cols-2 lg:gap-y-16">
            {features.map((feature) => (
              <div key={feature.name} className="relative pl-16">
                <dt className="text-base font-semibold leading-7 text-gray-900">
                  <div className="absolute left-0 top-0 flex h-10 w-10 items-center justify-center rounded-lg bg-indigo-600">
                    <feature.icon
                      className="h-6 w-6 text-white"
                      aria-hidden="true"
                    />
                  </div>
                  {feature.name}
                </dt>
                <dd className="mt-2 text-base leading-7 text-gray-600">
                  {feature.description}
                </dd>
              </div>
            ))}
          </dl>
        </div>
      </div>
    </div>
  );
}

