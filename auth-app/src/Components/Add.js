import { useState } from "react";
import axios from "axios";
export default function Add() {
  const [createForm, setCreateForm] = useState({
    lieu: "",
    chlorophylle: "",
    oxygene: "",
    azote: "",
    phosphor: "",
  });
  const createmarin = async (e) => {
    e.preventDefault();

    const res = await axios.post(
      "http://127.0.0.1:3001/Marin/ajouter",
      createForm
    );

    setCreateForm({
      lieu: "",
      chlorophylle: "",
      oxygene: "",
      azote: "",
      phosphor: "",
    });
  };
  const updateCreateFormField = (e) => {
    const { name, value } = e.target;

    setCreateForm({
      ...createForm,
      [name]: value,
    });
  };

  return (
    <>
      <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
        <div className="sm:mx-auto sm:w-full sm:max-w-sm">
          <img
            className="mx-auto h-20 w-20"
            src="logo.avif"
            alt="Your Company"
          />
          <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
            Add trix data
          </h2>
        </div>

        <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
          <form className="space-y-6"  onSubmit={createmarin}>
            <div>
              <label
                htmlFor="location"
                className="block text-sm font-medium leading-6 text-gray-900"
              >
                Location
              </label>
              <div className="mt-2">
                <input
                  id="lieu"
                  name="lieu"
                  type="text"
                  onChange={updateCreateFormField}
                  value={createForm.lieu}
                  required
                  className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                />
              </div>
            </div>
            <div>
              <label
                htmlFor="Chlorophylle"
                className="block text-sm font-medium leading-6 text-gray-900"
              >
                Chlorophylle
              </label>
              <div className="mt-2">
                <input
                  id="chlorophylle"
                  name="chlorophylle"
                  type="number"
                  autoComplete="text"
                  onChange={updateCreateFormField}
                  value={createForm.chlorophylle}
                  required
                  className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                />
              </div>
            </div>
            <div>
              <label
                htmlFor="Oxygene"
                className="block text-sm font-medium leading-6 text-gray-900"
              >
                Oxygene
              </label>
              <div className="mt-2">
                <input
                  id="oxygene"
                  name="oxygene"
                  type="number"
                  onChange={updateCreateFormField}
                  value={createForm.oxygene}
                  autoComplete="text"
                  required
                  className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                />
              </div>
            </div>
            <div>
              <label
                htmlFor="Azote"
                className="block text-sm font-medium leading-6 text-gray-900"
              >
                Azote
              </label>
              <div className="mt-2">
                <input
                  id="azote"
                  name="azote"
                  type="number"
                  onChange={updateCreateFormField}
                   value={createForm.azote}

                  autoComplete="text"
                  required
                  className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                />
              </div>
            </div>
            <div>
              <label
                htmlFor="Phosphore"
                className="block text-sm font-medium leading-6 text-gray-900"
              >
                Phosphore
              </label>
              <div className="mt-2">
                <input
                  id="phosphor"
                  name="phosphor"
                  type="number"
                  onChange={updateCreateFormField}
                  value={createForm.phosphor}

                  autoComplete="text"
                  required
                  className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                />
              </div>
            </div>
            <div>
              <button
                type="submit"
                className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
              >
                Submit
              </button>
            </div>
          </form>

          <p className="mt-10 text-center text-sm text-gray-500">
            Please be sure of the data .
            <a
              href="#"
              className="font-semibold leading-6 text-indigo-600 hover:text-indigo-500"
            >
              Your action is irreversible !!
            </a>
          </p>
        </div>
      </div>
    </>
  );
}