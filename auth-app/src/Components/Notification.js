import React from "react";
import { useState, useEffect } from "react";
import moment from "moment";
import { notification } from "../utils/AlerteUtils";

const Notification = ({ state, notif, setnotif }) => {
  const { web3, contract } = state;
  const [notifi, setNotifi] = useState([]);

  useEffect(() => {
    contract && notification(web3, contract, setNotifi);
    setnotif(0);
  }, [state]);

  const newnotif = [...notifi];
  newnotif.reverse();

  return (
    <div class="content-center ml-80">
      <h3 className="text-2xl text-gray-700 font-bold mb-6 ml-3">
        Latest News
      </h3>

      <ol>
        {newnotif.map((item) => (
          <li class="border-l-2 border-blue-400">
            <div class="md:flex flex-start">
              <div class="bg-blue-400 w-6 h-6 flex items-center justify-center rounded-full -ml-3.5">
                <svg
                  aria-hidden="true"
                  focusable="false"
                  data-prefix="fas"
                  class="text-white w-3 h-3"
                  role="img"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 448 512"
                >
                  <path
                    fill="currentColor"
                    d="M0 464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V192H0v272zm64-192c0-8.8 7.2-16 16-16h288c8.8 0 16 7.2 16 16v64c0 8.8-7.2 16-16 16H80c-8.8 0-16-7.2-16-16v-64zM400 64h-48V16c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v48H160V16c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v48H48C21.5 64 0 85.5 0 112v48h448v-48c0-26.5-21.5-48-48-48z"
                  ></path>
                </svg>
              </div>
              <div class="block p-6 rounded-lg shadow-lg bg-gray-100 max-w-md ml-6 mb-10">
                <div class="flex justify-between mb-4">
                  <a
                    href="#!"
                    class="font-medium text-blue-400 hover:text-blue-400 focus:text-blue-400 duration-300 transition ease-in-out text-sm"
                  >
                    {item.message}
                  </a>
                  <a
                    href="#!"
                    class="font-medium text-blue-400 hover:text-blue-400 focus:text-blue-400 duration-300 transition ease-in-out text-sm"
                  >
                    {moment.unix(item.startTime).fromNow()}
                  </a>
                </div>
                <p class="text-gray-700 mb-6">
                  Trying to find a solution to this problem...
                </p>
              </div>
            </div>
          </li>
        ))}
      </ol>
    </div>
  );
};

export default Notification;
