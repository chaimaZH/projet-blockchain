import React, { useEffect, useRef } from "react";
import Chart from "chart.js/auto";

const LineChart = ({ trixList, dateList }) => {
  const chartRef = useRef(null);

  useEffect(() => {
    var ticksStyle = {
      fontColor: "#495057",
      fontStyle: "bold",
    };
    var mode = "index";
    var intersect = true;
    const chartInstance = new Chart(chartRef.current, {
      type: "line",
      data: {
        labels: dateList,
        datasets: [
          {
            label: "Trix",
            data: trixList,
            backgroundColor: "transparent",
            borderColor: "#007bff",
            pointBorderColor: "#007bff",
            pointBackgroundColor: "#007bff",
            fill: false,
            pointHoverBackgroundColor: "#007bff",
            pointHoverBorderColor: "#007bff",
          },
        ],
      },
      options: {
        maintainAspectRatio: false,
        plugins: {
          tooltip: {
            mode: mode,
            intersect: intersect,
          },
          hover: {
            mode: mode,
            intersect: intersect,
          },
          legend: {
            display: true,
          },
        },
        scales: {
          y: {
            beginAtZero: true,
            grid: {
              display: true,
              lineWidth: "4px",
              color: "rgba(0, 0, 0, .2)",
              zeroLineColor: "transparent",
            },
            ticks: {
              ...ticksStyle,
              beginAtZero: true,
              suggestedMax: 200,
            },
          },
          x: {
            display: true,
            grid: {
              display: false,
            },
            ticks: ticksStyle,
          },
        },
      },
    });

    return () => {
      chartInstance.destroy();
    };
  }, [trixList, dateList]);

  return <canvas ref={chartRef} />;
};

export default LineChart;
