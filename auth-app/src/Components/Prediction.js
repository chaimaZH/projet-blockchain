import { useState, useEffect } from "react";
import axios from "axios";
import swal from "sweetalert";

export default function Prediction({
  state,
  SetblockNumber,
  setTransactionExecuted,
}) {
  const { web3, contract } = state;
  const [mois, setmois] = useState("");
  const [anne, setanne] = useState("");
  const [predict, setpredict] = useState("");
  const [load, setLoad] = useState(false);
  const [Msg, setMsg] = useState("");
  useEffect(() => {
    const { contract } = state;
  }, [state, setpredict , predict]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const params = { mois, anne };

    await axios
      .post("http://localhost:8085/prediction", params)
      .then((res) => {
        const data = res.data.data;
        const parameters = JSON.stringify(params);
        const date = String(params.anne + "-" + params.mois);
        const msg = `Prediction: ${data.prediction}\nParameters: ${parameters}`;
        setpredict(msg);
        reset();
        const sendTransaction = async () => {
          setMsg("");
          if (!load) {
            // Vérifier si une transaction est déjà en cours et si le compteur est inférieur à la longueur
            setLoad(true);
            try {
              web3.eth.getAccounts().then(async function (allAcounts) {
                if (allAcounts.length < 2) {
                  console.error("Not enough accounts available");
                  return;
                }

                const fromAddress = allAcounts[1];
                if (!web3.utils.isAddress(fromAddress)) {
                  console.error("Invalid from address:", fromAddress);
                  return;
                }

                const transaction = await contract.methods.insert(
                  String(date),
                  String("valeur predie"),
                  String("valeur predie"),
                  String("valeur predie"),
                  String("valeur predie"),
                  String("valeur predie"),
                  Math.trunc(data.prediction * 10)
                );

                const gasLimit = "6721975";
                await transaction
                  .send({ from: fromAddress, gas: gasLimit })
                  .then(function (receipt) {
                    console.log("Transaction receipt:", receipt);
                  })
                  .catch(function (error) {
                    console.error("Error sending transaction:", error);
                  });
                const latestBlock = await web3.eth.getBlockNumber();
                console.log("block number");
                console.log(latestBlock);
                SetblockNumber(latestBlock);
              });

              // await contract.methods.insert(String(marins[count].createdAt), String(marins[count].lieu), String(marins[count].chlorophylle), String(marins[count].oxygene), String(marins[count].azote), String(marins[count].phosphor), trixf).send({ from: String(compte), gas: "6721975" });

              console.log("Transaction is done");

              setLoad(false);
              setTransactionExecuted(true);
              // Définir l'état de chargement sur faux après la fin de la transaction
              swal({
                title: "Data Predicted !",
                text: msg,
                icon: "success",
              });
            } catch (error) {
              setLoad(false); // Définir l'état de chargement sur faux en cas d'erreur
              console.error(error);
            }
          } else {
            //setCount(0);
            console.log("....");
            setMsg("attente");
          }
        };

        sendTransaction(parameters, data.prediction);
        
      })
      .catch((error) => alert(`Error: ${error.message}`));

    
  };

  const reset = () => {
    setmois("");
    setanne("");
  };

  return (
    <>
      {/*
        This example requires updating your template:

        ```
        <html class="h-full bg-white">
        <body class="h-full">
        ```
      */}
      <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
        <div className="sm:mx-auto sm:w-full sm:max-w-sm">
          <img
            className="mx-auto h-20 w-20"
            src="logo.avif"
            alt="Your Company"
          />
          <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
            Predict Value TRIX
          </h2>
        </div>

        <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
          <form className="space-y-6" onSubmit={(e) => handleSubmit(e)}>
            <div>
              <label
                htmlFor="email"
                className="block text-sm font-medium leading-6 text-gray-900"
              >
                Month
              </label>
              <div className="mt-2">
                <input
                  id="month"
                  min="1"
                  max="12"
                  title="month"
                  type="number"
                  value={mois}
                  onChange={(e) => setmois(e.target.value)}
                  required
                  className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                />
              </div>
            </div>

            <div>
              <div className="flex items-center justify-between">
                <label
                  htmlFor="year"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Year
                </label>
              </div>
              <div className="mt-2">
                <input
                  id="year"
                  name="year"
                  type="number"
                  title="year"
                  value={anne}
                  onChange={(e) => setanne(e.target.value)}
                  required
                  className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                />
              </div>
            </div>

            <div>
              <button
                type="submit"
                className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
              >
                Predict
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}
