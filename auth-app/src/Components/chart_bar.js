import React, { useEffect, useRef } from "react";
import Chart from "chart.js/auto";

const LineChart_bar = ({ trixList, dateList }) => {
  const chartRef = useRef(null);

  useEffect(() => {
    var ticksStyle = {
      fontColor: "#495057",
      fontStyle: "bold",
    };
    var mode = "index";
    var intersect = true;
    const chartInstance = new Chart(chartRef.current, {
      type: "bar",
      data: {
        labels: dateList,
        datasets: [
          {
            label: "Trix",
            data: trixList,
            backgroundColor: "#007bff",
            borderColor: "#007bff",
          },
        ],
      },
      options: {
        maintainAspectRatio: false,
        tooltips: {
          mode: mode,
          intersect: intersect,
        },
        hover: {
          mode: mode,
          intersect: intersect,
        },
        legend: {
          display: false,
        },
        scales: {
          y: {
            display: true,
            gridLines: {
              display: false,
              color: "rgba(0, 0, 0, 0)",
            },
            grid: {
              display: false,
            },
            ticks: {
              ...ticksStyle,
              beginAtZero: true,

              // Include a dollar sign in the ticks
            },
          },
          x: {
            display: true,
            gridLines: {
              display: false,
              color: "rgba(0, 0, 0, 0)",
            },
            grid: {
              display: false,
            },
            ticks: ticksStyle,
          },
        },
      },
    });

    return () => {
      chartInstance.destroy();
    };
  }, [trixList, dateList]);

  return <canvas ref={chartRef} />;
};

export default LineChart_bar;
