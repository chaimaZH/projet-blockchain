import "./component/Glass.css";
import { useState } from "react";
import axios from "axios";
import LineChart from "./chart";
import LineChart_bar from "./chart_bar";
import React, { useEffect, useRef } from "react";
import { Chart, registerables } from "chart.js";

Chart.register(...registerables);

function Dashboard({ state, SetblockNumber, setTransactionExecuted }) {
  const { web3, contract } = state;
  //console.log(state)
  const [marin, setmarin] = useState([]);
  const [trixList, setTrixList] = useState([]);
  useEffect(() => {
    const { contract } = state;
    async function marins() {
      const marin = await contract.methods.recherche().call();
      setmarin(marin);
      console.log("Marins is done");
    }
    contract && marins();
  }, [state]);
  const newmarin = [...marin];
  newmarin.reverse();
  // Cette fonction est appelée lorsque la liste de données est mise à jour
  const [dateList, setdateList] = useState([]);

  // Cette fonction est appelée lorsque la liste de données est mise à jour
  useEffect(() => {
    const newTrixList = [];
    const newDateList = [];
    for (let i = 0; i < marin.length; i++) {
      const marineData = marin[i];

      const trixValue = marineData.trix / 10;
      const dateValue = marineData.date.substr(0, 7);
      newTrixList.push(trixValue);
      newDateList.push(dateValue);
      if (i >= 5) {
        newTrixList.shift();
        newDateList.shift();
      }
    }

    setTrixList(newTrixList);
    setdateList(newDateList); // Met à jour la liste "trixList" avec les nouvelles valeurs
  }, [marin]);
  // Cette fonction est appelée lorsque la liste de données est mise à jour

  const [mois, setmois] = useState("");
  const [anne, setanne] = useState("");
  const [predict, setpredict] = useState("");
  const [load, setLoad] = useState(false);
  const [Msg, setMsg] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    const params = { mois, anne };

    axios
      .post("http://localhost:8085/prediction", params)
      .then((res) => {
        const data = res.data.data;
        const parameters = JSON.stringify(params);
        const date = String(params.anne + "-" + params.mois);
        const msg = `{Prediction: ${data.prediction}\nParameters: ${parameters}`;
        setpredict(msg);
        //alert(msg)
        reset();
        const sendTransaction = async () => {
          setMsg("");
          if (!load) {
            // Vérifier si une transaction est déjà en cours et si le compteur est inférieur à la longueur
            setLoad(true);
            try {
              web3.eth.getAccounts().then(async function (allAcounts) {
                if (allAcounts.length < 2) {
                  console.error("Not enough accounts available");
                  return;
                }

                const fromAddress = allAcounts[1];
                if (!web3.utils.isAddress(fromAddress)) {
                  console.error("Invalid from address:", fromAddress);
                  return;
                }

                const transaction = await contract.methods.insert(
                  String(date),
                  String("valeur predie"),
                  String("valeur predie"),
                  String("valeur predie"),
                  String("valeur predie"),
                  String("valeur predie"),
                  Math.trunc(data.prediction * 10)
                );

                const gasLimit = "6721975";
                transaction
                  .send({ from: fromAddress, gas: gasLimit })
                  .then(function (receipt) {
                    console.log("Transaction receipt:", receipt);
                  })
                  .catch(function (error) {
                    console.error("Error sending transaction:", error);
                  });
                const latestBlock = await web3.eth.getBlockNumber();
                console.log(latestBlock);
                SetblockNumber(latestBlock);
              });

              // await contract.methods.insert(String(marins[count].createdAt), String(marins[count].lieu), String(marins[count].chlorophylle), String(marins[count].oxygene), String(marins[count].azote), String(marins[count].phosphor), trixf).send({ from: String(compte), gas: "6721975" });

              console.log("Transaction is done");

              setLoad(false);
              setTransactionExecuted(true);
              // Définir l'état de chargement sur faux après la fin de la transaction
            } catch (error) {
              setLoad(false); // Définir l'état de chargement sur faux en cas d'erreur
              console.error(error);
            }
          } else {
            //setCount(0);
            console.log("....");
            setMsg("attente");
          }
        };

        sendTransaction(parameters, data.prediction);
      })
      .catch((error) => alert(`Error: ${error.message}`));
  };

  const reset = () => {
    setmois("");
    setanne("");
  };

  return (
    <div>
      {/* Content Wrapper. Contains page content */}
      <div className="content-wrapper">
        {/* Content Header (Page header) */}
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6"></div>
              {/* /.col */}
              <div className="mx-auto max-w-2xl lg:text-center">
                <p className="mt-2 text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
                  Navigating Environmental Data
                </p>
                <p className="mt-6 text-lg leading-8 text-gray-600">
                  A Pollution Overview{" "}
                </p>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item"></li>
                </ol>
              </div>
              {/* /.col */}
            </div>
            {/* /.row */}
          </div>
          {/* /.container-fluid */}
        </div>
        {/* /.content-header */}
        {/* Main content */}
        <div className="content">
          <div className="container-fluid">
            <div className="row">
              <div className="col-lg-6">
                <div className="card">
                  <div className="card-header border-0 custom-background">
                    <div className="d-flex justify-content-between">
                      <h3 className="card-title" style={{ fontWeight: "bold" }}>
                        Graphic curve Pollution Index
                      </h3>{" "}
                    </div>
                  </div>
                  <div className="card-body">
                    <div className="d-flex">
                      <p className="d-flex flex-column">
                        <span>Pollution Index</span>
                      </p>
                    </div>
                    {/* /.d-flex */}
                    <div className="position-relative mb-4">
                      <LineChart trixList={trixList} dateList={dateList} />
                    </div>
                    <div className="d-flex flex-row justify-content-end">
                      <span className="mr-2">
                        <i className="fas fa-square text-primary" /> Date
                      </span>
                    </div>
                  </div>
                </div>
                {/* /.card */}

                {/* /.card */}
              </div>
              {/* /.col-md-6 */}
              <div className="col-lg-6">
                <div className="card">
                  <div className="card-header border-0 custom-background">
                    <div className="d-flex justify-content-between">
                      <h3 className="card-title" style={{ fontWeight: "bold" }}>
                        Histogram Pollution Index
                      </h3>
                    </div>
                  </div>
                  <div className="card-body">
                    <div className="d-flex">
                      <p className="d-flex flex-column">
                        <span>Pollution Index</span>
                      </p>
                    </div>
                    {/* /.d-flex */}
                    <div className="position-relative mb-4">
                      {/**/}
                      <LineChart_bar trixList={trixList} dateList={dateList} />
                    </div>
                    <div className="d-flex flex-row justify-content-end">
                      <span className="mr-2">
                        <i className="fas fa-square text-primary" /> Date
                      </span>
                    </div>
                  </div>
                </div>
                {/* /.card */}
              </div>
            </div>
            {/* /.row */}
          </div>
          {/* /.container-fluid */}
        </div>
        {/* /.content */}
      </div>
      {/* /.content-wrapper */}
      {/* Control Sidebar */}
      <aside className="control-sidebar control-sidebar-dark">
        {/* Control sidebar content goes here */}
      </aside>
      {/* /.control-sidebar */}
    </div>
  );
}

export default Dashboard;
