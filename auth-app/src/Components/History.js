import { useState, useEffect } from "react";
import React from "react";
import marins from "../utils/MarinUtils";
import "@fortawesome/fontawesome-free/css/all.css";

const History = ({ state }) => {
  // Utilisez variableLocale dans cette page comme vous le souhaitez
  const [marin, setmarin] = useState([]);
  useEffect(() => {
    const { contract } = state;

    contract && marins(contract, setmarin);
  }, [state]);
  const newmarin = [...marin];
  newmarin.reverse();

  return (
    <section class="items-center py-24 bg-gray-100 font-poppins ">
      <div class="justify-center max-w-6xl px-4 py-4 mx-auto lg:py-8 md:px-6">
        <div class="max-w-xl mx-auto">
          <div class="text-center ">
            <div class="relative flex flex-col items-center">
              <h1 class="text-6xl font-bold leading-tight "> History</h1>
              <div class="flex w-24 mt-1 mb-10 overflow-hidden rounded">
                <div class="flex-1 h-2 bg-blue-200"></div>
                <div class="flex-1 h-2 bg-blue-400"></div>
                <div class="flex-1 h-2 bg-blue-600"></div>
              </div>
            </div>
          </div>
        </div>
        {newmarin.map((item) => (
          <div class="w-full mx-auto lg:max-w-3xl">
            <div class="relative flex justify-between">
              <div class="flex flex-col items-center w-10 mr-4 md:w-24">
                <div>
                  <div class="flex items-center justify-center w-8 h-8 bg-blue-200 rounded-full ">
                    <div class="w-4 h-4 bg-blue-600 rounded-full "></div>
                  </div>
                </div>
                <div class="w-px h-full bg-blue-300 "></div>
              </div>
              <div>
                <h2 class="inline-block px-4 py-2 mb-4 text-xs font-medium text-gray-100 bg-gradient-to-r from-blue-500 to-blue-900 ">
                  {String(item.date)}
                </h2>
                <div class="relative flex-1 mb-10 bg-white border-b-4 border-blue-200 shadow ">
                  <div class="relative z-20 p-6">
                    <p class="mb-2 text-xl font-bold text-gray-600">
                      Trix value :{" "}
                      <span class="text-blue-600">
                        {String(item.trix / 10)}
                      </span>
                    </p>

                    <div class="flex flex-col justify-center items-center">
                      <div class=" md:min-w-[700px] xl:min-w-[800px] mt-3 grid grid-cols-1 gap-1 md:grid-cols-2 lg:grid-cols-4 2xl:grid-cols-3 3xl:grid-cols-6">
                        <div class="relative flex flex-grow !flex-row flex-col items-center rounded-[10px] rounded-[10px] border-[1px] border-gray-200 bg-white bg-clip-border shadow-md shadow-[#F3F3F3] ">
                          <div class="ml-[18px] flex h-[90px] w-auto flex-row items-center">
                            <div class="rounded-full bg-lightPrimary p-3 ">
                              <span class="flex items-center text-brand-500 ">
                                <i className="fas fa-seedling text-3xl"></i>
                              </span>
                            </div>
                          </div>
                          <div class="h-50 ml-4 flex w-auto flex-col justify-center">
                            <p class="font-dm text-sm font-medium text-gray-600">
                              Chlorophylle
                            </p>
                            <h4 class="text-xl font-bold text-navy-700 ">
                              {String(item.chlorophylle)} %
                            </h4>
                          </div>
                        </div>
                        <div class="relative flex flex-grow !flex-row flex-col items-center rounded-[10px] rounded-[10px] border-[1px] border-gray-200 bg-white bg-clip-border shadow-md shadow-[#F3F3F3] ">
                          <div class="ml-[18px] flex h-[90px] w-auto flex-row items-center">
                            <div class="rounded-full bg-lightPrimary p-3 ">
                              <span class="flex items-center text-brand-500 ">
                                <i className="fas fa-thin fa-atom text-3xl" />
                              </span>
                            </div>
                          </div>
                          <div class="h-50 ml-4 flex w-auto flex-col justify-center">
                            <p class="font-dm text-sm font-medium text-gray-600">
                              Oxygene
                            </p>
                            <h4 class="text-xl font-bold text-navy-700 ">
                              {String(item.oxygene)} %
                            </h4>
                          </div>
                        </div>
                        <div class="relative flex flex-grow !flex-row flex-col items-center rounded-[10px] rounded-[10px] border-[1px] border-gray-200 bg-white bg-clip-border shadow-md shadow-[#F3F3F3] ">
                          <div class="ml-[18px] flex h-[90px] w-auto flex-row items-center">
                            <div class="rounded-full bg-lightPrimary p-3 ">
                              <span class="flex items-center text-brand-500 ">
                                <i className="fas fa-duotone fa-fill-drip text-3xl" />
                              </span>
                            </div>
                          </div>
                          <div class="h-50 ml-4 flex w-auto flex-col justify-center">
                            <p class="font-dm text-sm font-medium text-gray-600">
                              Azote
                            </p>
                            <h4 class="text-xl font-bold text-navy-700 ">
                              {String(item.azote)} %
                            </h4>
                          </div>
                        </div>
                        <div class="relative flex flex-grow !flex-row flex-col items-center rounded-[10px] rounded-[10px] border-[1px] border-gray-200 bg-white bg-clip-border shadow-md shadow-[#F3F3F3] ">
                          <div class="ml-[18px] flex h-[90px] w-auto flex-row items-center">
                            <div class="rounded-full bg-lightPrimary p-3 ">
                              <span class="flex items-center text-brand-500 ">
                                <i class="fas fa-duotone fa-flask text-3xl"></i>
                              </span>
                            </div>
                          </div>
                          <div class="h-50 ml-4 flex w-auto flex-col justify-center">
                            <p class="font-dm text-sm font-medium text-gray-600">
                              Phosphore
                            </p>
                            <h4 class="text-xl font-bold text-navy-700 ">
                              {String(item.phosphore)} %
                            </h4>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="ht  mt-8 grid place-items-center">
                      <p className="text-gray-700 ">
                        <h3 className=" content-center justify-center">
                          <span class="text-blue-600">{String(item.lieu)}</span>
                          : {String(item.message)}
                        </h3>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
        <div className="w-4 h-4 ml-44 bg-blue-600 rounded-full "></div>
      </div>
    </section>
  );
};

export default History;
