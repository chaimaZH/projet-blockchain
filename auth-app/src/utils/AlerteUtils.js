import { Notyf } from "notyf";
import "notyf/notyf.min.css";
import { loadBlockchainData, loadWeb3 } from "../Web3helpers";

const loadAccounts = async () => {
  loadWeb3();
  let { auth, accounts } = await loadBlockchainData();

  console.log("auth", auth);
  const addresses = await auth.methods.getAllUserAddresses().call();
  return addresses;
};

const notyf = new Notyf({
  duration: 10000,
  position: {
    x: "right",
    y: "top",
  },
  types: [
    {
      type: "tres bon etat",
      background: "blue",
      duration: 10000,
      icon: false,
    },
    {
      type: "bon etat",
      background: "green",
      duration: 10000,
      icon: false,
    },
    {
      type: "etat moyen",
      background: "yellow",
      duration: 10000,
      dismissible: true,
      icon: false,
    },
    {
      type: "etat mediocre",
      background: "Orange",
      duration: 10000,
      dismissible: true,
      icon: false,
    },
    {
      type: "etat mauvais",
      background: "indianred",
      duration: 10000,
      dismissible: true,
    },
  ],
});

const alerte = async (state, blockNumber, setnotif, notif) => {
  const { contract } = state;

  //console.log(blockNumber);
  const events = await contract.getPastEvents("alerte", {
    fromBlock: blockNumber,
    toBlock: blockNumber,
  });
  //console.log(events);
  if (events.length > 0) {
    const returnValues = events[0].returnValues;
    // console.log(returnValues);
    const Event = returnValues._message;
    //console.log(Event);
    console.log("***");

    notyf.open({
      type: Event,
      message: Event,
      dismissible: true,
    });

    if (Event === "etat mauvais") {
      sendAlert(state, setnotif, notif);
    }
  }
};
const sendAlert = async (state, setnotif, notif) => {
  const { contract, web3 } = state;
  web3.eth.getAccounts().then(async function (allAcounts) {
    if (allAcounts.length < 2) {
      console.error("Not enough accounts available");
      return;
    }
    let addresses = loadAccounts();
    let l = [];
    l= await addresses.then((result) => {
      console.log(result);
      for (let value of result) {
        l.push(value);
      }
      return l;
    });

    console.log(l.length);
    for (let i = 0; i < l.length; i++) {

      const fromAddress = allAcounts[1];
      const fromAddress_to = l[i];

      if (!web3.utils.isAddress(fromAddress)) {
        console.error("Invalid from address:", fromAddress);
        return;
      }
      if (!web3.utils.isAddress(fromAddress_to)) {
        console.error("Invalid from address_to:", fromAddress_to);
        return;
      }

      const alert = contract.methods.alertenvoyer(
        web3.utils.toChecksumAddress(String(fromAddress_to)),
        "etat mauvais"
      );
      const gasLimit = "6721975";
      await alert
        .send({ from: fromAddress, gas: gasLimit })
        .then(function (receipt) {
          console.log("Alert  receipt:", receipt);
        })
        .catch(function (error) {
          console.error("Error sending Alert:", error);
        });
    }
    setnotif(notif + 1);
  });
  
};
export default alerte;

export async function notification(web3, contract, setNotifi) {
  try {
    console.log("notif");
    web3.eth.getAccounts().then(async function (allAcounts) {
      if (allAcounts.length < 2) {
        console.error("Not enough accounts available");
        return;
      }
		let account =localStorage.getItem("account");
      const fromAddress = account;
      if (!web3.utils.isAddress(fromAddress)) {
        console.error("Invalid from address:", fromAddress);
        return;
      }
      const notif = await contract.methods.getAlerts(fromAddress).call();
      console.log("notif is done *************");
      console.log(notif);
      console.log("***********");

      setNotifi(notif);
    });
  } catch (error) {
    console.error(error);
  }
}
