const sendTransaction = async (
    setMessage,
    setLoading,
    loading,
    count,
    longeur,
    state,
    marins,
    SetblockNumber,
    setCount,
    setTransactionExecuted
  ) => {
    console.log(state);
    const { contract } = state;
    setMessage("");
    if (!loading && count < longeur) {
      // Vérifier si une transaction est déjà en cours et si le compteur est inférieur à la longueur
      setLoading(true);
      try {
        //console.log(count);
  
        const trix = Math.log10(
          (parseFloat(marins[count].chlorophylle) *
            parseFloat(marins[count].oxygene) *
            parseFloat(marins[count].azote) *
            parseFloat(marins[count].phosphor) +
            1.5) /
            1.2
        );
        const trixf = Math.trunc(trix * 10);
        const { web3 } = state;
        web3.eth.getAccounts().then(async function (allAcounts) {
          if (allAcounts.length < 2) {
            console.error("Not enough accounts available");
            return;
          }
  
          const fromAddress = allAcounts[1];
          if (!web3.utils.isAddress(fromAddress)) {
            console.error("Invalid from address:", fromAddress);
            return;
          }
  
          const transaction = contract.methods.insert(
            String(marins[count].createdAt),
            String(marins[count].lieu),
            String(marins[count].chlorophylle),
            String(marins[count].oxygene),
            String(marins[count].azote),
            String(marins[count].phosphor),
            trixf
          );
  
          const gasLimit = "6721975";
          await transaction
            .send({ from: fromAddress, gas: gasLimit })
            .then(function (receipt) {
              console.log("Transaction receipt:", receipt);
            })
            .catch(function (error) {
              console.error("Error sending transaction:", error);
            });
          const latestBlock = await web3.eth.getBlockNumber();
          SetblockNumber(latestBlock);
        });
  
        console.log("Transaction is done");
        setCount(count + 1);
        setLoading(false); // Définir l'état de chargement sur faux après la fin de la transaction
        setTransactionExecuted(true);
      } catch (error) {
        setLoading(false); // Définir l'état de chargement sur faux en cas d'erreur
        console.error(error);
      }
    }
  };
  export default sendTransaction;
  