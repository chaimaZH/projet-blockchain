// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract Auth {
    address public owner;

    uint public userCount = 0;

    mapping(string => user) public usersList;
    string[] public userEmails;

    struct user {
        string username;
        string email;
        string password;
        address adress;
    }

    constructor() {
        owner = msg.sender;
    }

    // events

    event userCreated(
        string username,
        string email,
        string password,
        address adress
    );

    function getOwner() external view returns (address) {
        return owner;
    }

    function createUser(
        string memory _username,
        string memory _email,
        string memory _password,
        address _adress
    ) public {
        userCount++;
        usersList[_email] = user(_username, _email, _password, _adress);
        userEmails.push(_email);
        emit userCreated(_username, _email, _password, _adress);
    }

    function getAllUserAddresses() public view returns (address[] memory) {
        address[] memory allUserAddresses = new address[](userCount);

        for (uint i = 0; i < userCount; i++) {
            allUserAddresses[i] = usersList[userEmails[i]].adress;
        }
        return allUserAddresses;
    }
}
