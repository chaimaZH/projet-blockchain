// SPDX-License-Identifier: MIT
pragma solidity >=0.5.0 <0.9.0;
contract Marin {
    
    event alerte(string _message);
    struct marin {
        string date;
        string lieu;
        string chlorophylle;
        string oxygene;
        string azote;
        string phosphore;
        uint trix;
        string message;

    }
    marin [] marins ;
    struct Alert { 
        string message;
        uint startTime;
        bool isRead;
    }
    mapping(address => Alert[]) Alerts;
   
    function insert(string memory _date ,string memory _lieu,string memory _chlorophylle,string memory _oxygene,string memory _azote,string memory _phosphore,uint _trix) public returns (bool){
        if (_trix>=0){
            if (_trix<40){
            marins.push(marin(_date, _lieu,_chlorophylle,_oxygene,_azote,_phosphore,_trix,"tres bon etat"));
                            emit alerte ('tres bon etat');

            }
            else if (_trix <50){
                marins.push(marin(_date, _lieu,_chlorophylle,_oxygene,_azote,_phosphore,_trix," bon etat"));
                                emit alerte ('bon etat');

            }
            else if (_trix <60) {
                marins.push(marin(_date, _lieu,_chlorophylle,_oxygene,_azote,_phosphore,_trix,"etat moyen"));  
                                emit alerte ('etat moyen');

                 }
            else if(_trix <80){
               marins.push(marin(_date, _lieu,_chlorophylle,_oxygene,_azote,_phosphore,_trix,"etat mediocre"));
                emit alerte ('etat mediocre');

            }
            else {
                marins.push(marin(_date, _lieu,_chlorophylle,_oxygene,_azote,_phosphore,_trix,"etat mauvais"));
                emit alerte ('etat mauvais');
            }
        }
        else {
            emit alerte('erreur');
        }

       return true;

    }
   
    function recherche() public view returns (marin[] memory) {
        return marins;
    }
    function getdata(uint _nomber) public view returns (marin memory){
          return marins[_nomber];
    }

    function alertenvoyer ( address envoyer_to ,string memory _message) public{
         Alert memory newAlert = Alert(_message, block.timestamp, false);
         Alerts[envoyer_to].push(newAlert); 
    }
    function markAlertAsRead(address envoyer_to, uint index) public {
      require(index < Alerts[envoyer_to].length, "Invalid alert index");
      Alerts[envoyer_to][index].isRead = true;
    }

function getUnreadAlerts(address envoyer_to) public view returns (Alert[] memory) {
   uint unreadCount = 0;
    for (uint i = 0; i < Alerts[envoyer_to].length; i++) {
        if (!Alerts[envoyer_to][i].isRead) {
            unreadCount++;
        }
    }
    Alert[] memory unreadAlerts = new Alert[](unreadCount);
    uint index = 0;
    for (uint i = 0; i < Alerts[envoyer_to].length; i++) {
        if (!Alerts[envoyer_to][i].isRead) {
            unreadAlerts[index] = Alerts[envoyer_to][i];
            index++;
        }
    }
    return unreadAlerts;
}
function getAlerts(address envoyer_to) public view returns (Alert[] memory) {
    return Alerts[envoyer_to];
}

    

   
}