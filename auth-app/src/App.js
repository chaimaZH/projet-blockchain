import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import { useEffect, useState } from "react"; // Import useState and useEffect

import SignIn from "./Components/Signin";
import SignUp from "./Components/Signup";
import Home from "./Components/Home";
import Add from "./Components/Add";
import History from "./Components/History";
import Notification from "./Components/Notification";
import Prediction from "./Components/Prediction";
import Navbar from "./Components/Navbar";
import alerte from "./utils/AlerteUtils";
import sendTransaction from "./utils/TransactionUtils";
import axios from "axios";
import Marin from "./build/contracts/Marin.json";
import Auth from "./build/contracts/Auth.json";
import Web3 from "web3/dist/web3.min.js";
import Footer from "./Components/Footer";
import About_Us from "./Components/About_Us";
import Dashboard from "./Components/Dashboard";
import Unauthorized from "./Components/Unauthorized";
import { getOwner } from "./utils/MarinUtils";

function App() {
  // Use useState to manage the email state
  const [email, setEmail] = useState(localStorage.getItem("email"));
  const updateEmail = (newValue) => {
    setEmail(newValue);
  };

  const [marins, setmarins] = useState(null);

  const [notif, setnotif] = useState(
    Number(localStorage.getItem("notif")) || 0
  );

  const [count, setCount] = useState(
    Number(localStorage.getItem("count")) || 0
  );

  const [Message, setMessage] = useState("");
  const [longeur, setlongeur] = useState();

  const [loading, setLoading] = useState(false);
  const [blockNumber, SetblockNumber] = useState();

  const [transactionExecuted, setTransactionExecuted] = useState(false);
  const [owner, setOwner] = useState(null);
  const [account, setAccount] = useState(localStorage.getItem("account"));
  useEffect(() => {
    localStorage.setItem("count", count);
  }, [count]);

  useEffect(() => {
    listermarins();
  }, [marins]);

  useEffect(() => {
    localStorage.setItem("notif", notif);
  }, [notif]);

  const listermarins = async () => {
    const res = await axios.get("http://127.0.0.1:3001/Marin/lister");
    const longeur = res.data.marins.length;
    setlongeur(longeur);
    setmarins(res.data.marins);
  };

  const [state, setState] = useState({
    web3: null,
    contract: null,
  });

  useEffect(() => {
    const provider = new Web3.providers.HttpProvider("HTTP://127.0.0.1:7545");

    async function template() {
      const web3 = new Web3(provider);
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = Marin.networks[networkId];

      const contract = new web3.eth.Contract(
        Marin.abi,
        deployedNetwork.address
      );
      await  getOwner(
        new web3.eth.Contract(Auth.abi, Auth.networks[networkId].address),
        setOwner
      );
      console.log(owner);
      setState({ web3: web3, contract: contract });
    }
    provider && template();
  }, [owner]);

  
  /***********Alerte */

  useEffect(() => {
    sendTransaction(
      setMessage,
      setLoading,
      loading,
      count,
      longeur,
      state,
      marins,
      SetblockNumber,
      setCount,
      setTransactionExecuted
    );
  }, [count, longeur, loading, transactionExecuted, state]);
  /*********** */

  useEffect(() => {
    if (transactionExecuted) {
      console.log(transactionExecuted);
      alerte(state, blockNumber, setnotif, notif);
      setTransactionExecuted(false);
    }
  }, [blockNumber]);

  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route
            exact
            path="/"
            element={<SignIn updateEmail={updateEmail} />}
          />
          <Route
            path="/Signup"
            element={<SignUp updateEmail={updateEmail} />}
          />
          <Route
            path="/Home"
            element={
              email ? (
                <>
                  <Navbar
                    notif={notif}
                    setnotif={setnotif}
                    state={state}
                    updateEmail={updateEmail}
                  />{" "}
                  <Dashboard state={state} />
                  <Home
                    state={state}
                    setTransactionExecuted={setTransactionExecuted}
                    SetblockNumber={SetblockNumber}
                  />
                  <Footer state={state} />{" "}
                </>
              ) : (
                <Navigate to="/" />
              )
            }
          />
          {account === owner ? (
            <Route
              path="/Add"
              element={
                email ? (
                  <>
                    <Navbar
                      notif={notif}
                      setnotif={setnotif}
                      state={state}
                      updateEmail={updateEmail}
                    />
                    <Add />
                    <Footer state={state} />{" "}
                  </>
                ) : (
                  <Navigate to="/" />
                )
              }
            />
          ) : (
            <Route
              path="/Add"
              element={
                email ? (
                  <>
                    <Navbar
                      notif={notif}
                      setnotif={setnotif}
                      state={state}
                      updateEmail={updateEmail}
                    />
                    <Unauthorized />
                    <Footer state={state} />{" "}
                  </>
                ) : (
                  <Navigate to="/" />
                )
              }
            />
          )}
          <Route
            path="/History"
            element={
              email ? (
                <>
                  <Navbar
                    notif={notif}
                    setnotif={setnotif}
                    state={state}
                    updateEmail={updateEmail}
                  />
                  <History state={state} />
                  <Footer state={state} />{" "}
                </>
              ) : (
                <Navigate to="/" />
              )
            }
          />
          <Route
            path="/Notification"
            element={
              email ? (
                <>
                  <Navbar
                    notif={notif}
                    setnotif={setnotif}
                    state={state}
                    updateEmail={updateEmail}
                  />{" "}
                  <Notification
                    state={state}
                    notif={notif}
                    setnotif={setnotif}
                  />
                  <Footer state={state} />{" "}
                </>
              ) : (
                <Navigate to="/" />
              )
            }
          />
          {account === owner ? (
            <Route
              path="/Prediction"
              element={
                email ? (
                  <>
                    <Navbar
                      notif={notif}
                      setnotif={setnotif}
                      state={state}
                      updateEmail={updateEmail}
                    />
                    <Prediction
                      state={state}
                      setTransactionExecuted={setTransactionExecuted}
                      SetblockNumber={SetblockNumber}
                    />{" "}
                    <Footer state={state} />{" "}
                  </>
                ) : (
                  <Navigate to="/" />
                )
              }
            />
          ) : (
            <Route
              path="/Prediction"
              element={
                email ? (
                  <>
                    <Navbar
                      notif={notif}
                      setnotif={setnotif}
                      state={state}
                      updateEmail={updateEmail}
                    />
                    <Unauthorized/>
                    <Footer state={state} />{" "}
                  </>
                ) : (
                  <Navigate to="/" />
                )
              }
            />
          )}
          <Route
            path="/About_Us"
            element={
              email ? (
                <>
                  <Navbar
                    notif={notif}
                    setnotif={setnotif}
                    state={state}
                    updateEmail={updateEmail}
                  />
                  <About_Us state={state} />
                  <Footer state={state} />{" "}
                </>
              ) : (
                <Navigate to="/" />
              )
            }
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
