# Importing necessary libraries
import uvicorn
import pickle
from pydantic import BaseModel
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

# Initializing the fast API server
app = FastAPI()

origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8085",
    "http://localhost:3000",
    "http://localhost:3002",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Loading up the trained model
model = pickle.load(open('auth-app\model\marin.pkl', 'rb'))

# Defining the model input types
class Candidate(BaseModel):
    mois: int
    anne: int


# Setting up the home route
@app.get("/")
def read_root():
    return {"data": "Welcome to trix prediction model"}

# Setting up the prediction route
@app.post("/prediction/")
async def get_predict(data: Candidate):
    sample = [[
        data.mois,
        data.anne,
    ]]

    hired = model.predict(sample).tolist()[0]


    return {
        "data": {
            'prediction': hired,
            
        }
    }

# Configuring the server host and port
if __name__ == '__main__':
    uvicorn.run(app, port=8085, host='0.0.0.0')